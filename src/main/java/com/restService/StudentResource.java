package com.restService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//@Controller
@RestController
public class StudentResource {

	@Autowired
	private StudentRepository studentRepository;

	@GetMapping("/")
	public List<Student> retrieveAllStudents() {
		return studentRepository.findAll();
	}
	
//	@GetMapping("/")
//	public String index() {
//		return "index";
//	}
	
	
//	@GetMapping("/students")
//	public List<Student> retrieveAllStudents() {
//		return studentRepository.findAll();
//	}
		
}
